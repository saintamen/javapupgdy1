package zadanie19;

import java.util.Scanner;

public class MainZadanie19BezBreak {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        double sum = 0;
        int liczba = -1; // do załadowania od uzytkownika
        int licznikLiczb = 0;
        while((liczba = scanner.nextInt()) != 0){
            // sprawdzamy max
            if (liczba > max) {
                max = liczba;
            }
            // sprawdzamy min
            if (liczba < min) {
                min = liczba;
            }
            // sumujemy do sredniej (na potem)
            sum += liczba;
            licznikLiczb ++;
        }
        System.out.println("Min+max = " + (min + max));
        System.out.println("Srednia = " + sum/licznikLiczb);
    }
}
