package zadanie28;

import java.util.Scanner;

public class MainZadanie28 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();

        boolean[][] tab = new boolean[liczba][liczba];
        for (int i = 0; i < liczba; i++) {
            for (int j = 0; j < liczba; j++) {
                // sa wzglednie pierwsze
                tab[i][j] = true;
            }
        }

        for (int i = 0; i < liczba; i++) {
            for (int j = 0; j < liczba; j++) {
                // musimy sprawdzic tutaj czy (j+1) i (i+1) maja wspolny dzielnik
                // jesli nie sa to zaznaczamy komorke na false
                for (int k = 2; k < liczba; k++) {
                    if (((j + 1) % k == 0) && ((i + 1) % k == 0)) {
                        // jesli znalezlismy wspolny dzielnik
                        tab[i][j] = false;
                        break;
                    }
                }
            }
        }

        // wypisanie
        for (int i = 0; i < liczba; i++) {
            for (int j = 0; j < liczba; j++) {
                if(tab[i][j]){
                    System.out.print("+ ");
                }else{
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }
}
