package zadanie8;

public class MainZadanie8 {
    public static void main(String[] args) {
        int a = 1;
        int b = 5;
        int c = 100;
        System.out.println("a=" + a + " b=" + b + " c=" + c);

        int max = a;
        int min = a;
        // 3 elementowa tablica
        int[] tab = new int[]{ a, b, c };
        for (int i = 1; i < tab.length; i++) {
            if(max < tab[i]){ // jeśli i-ty element tablicy jest wiekszy
                                // niz obecne maximum
                max = tab[i];   // to zamien max na ten element tablicy
            }
            if(min > tab[i]){ // jeśli i-ty element tablicy jest mniejszy
                                // niz obecne minimum
                min = tab[i];   // to zamien min na ten element tablicy
            }
        }

        // opcja druga
//        int min = a;
//        int max = a;
//        //
//
//        // znajdz max z 3 liczb
//        if (b > max) {
//            max = b;
//        }
//        if (c > max) {
//            max = c;
//        }
//
//        // znajdz min z 3 liczb
//        if (b < min) {
//            min = b;
//        }
//        if (c < min) {
//            min = c;
//        }
//
//        System.out.println("Max: " + max + " min: " + min);

    }
}
