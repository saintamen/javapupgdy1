import java.util.Random;

public class RandomPrzyklad {
    public static void main(String[] args) {
        // posługując się Math.random()
        double randomOdZeraDo1 = Math.random(); // 0.0 - 1.0
        int zakres = 100;// do jakiej liczby wylosowac (gorna granica)
        double losowa = randomOdZeraDo1*zakres; // 0.0 - 100.0

        // "2 * (3.4 - (-7)/2)*(a-2)/(b-1)))
        // "2) * (3.4 - (-7)/2)*(a-2)/(b-1))
        // posługując się generatorem (Random) javy
        Random generator = new Random();
        int losowaZPrzedzialu = generator.nextInt(zakres);
        // 0 - 100

        int gornaGranica = 100;
        int dolnaGranica = 50;


        int dlugoscPrzedzialu = gornaGranica-dolnaGranica;
        int losowaPrzedzial = generator.nextInt(dlugoscPrzedzialu); // wylosuje liczbe od 0 do 50
        losowaPrzedzial+= dolnaGranica; // przesuwam przedział o dolna granice
    }
}
