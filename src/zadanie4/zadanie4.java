package zadanie4;

public class zadanie4 {
    public static void main(String[] args) {
        boolean jest_cieplo = true;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = true;

        boolean ubieram_sie_cieplo = false;
        boolean biore_parasol = false;
        boolean ubieram_kurtke = false;

//        if(!jest_cieplo || wieje_wiatr){
        // przy lub -> którykolwiek musi być true
        if(jest_cieplo == false || wieje_wiatr == true){
            ubieram_sie_cieplo = true;
        }
        // !true && !true
        // false && false
        // false
//        if(!swieci_slonce && !wieje_wiatr){
        if(swieci_slonce == false && wieje_wiatr == false){
            biore_parasol = true;
        }
//        if(wieje_wiatr && !swieci_slonce && !jest_cieplo){
        if(wieje_wiatr == true && swieci_slonce == false && jest_cieplo == false){
            ubieram_kurtke = true;
        }

        System.out.println("ubieram sie cieplo = " + ubieram_sie_cieplo);
        System.out.println("parasol = " + biore_parasol);
        System.out.println("kurtka = " + ubieram_kurtke);
    }
}
