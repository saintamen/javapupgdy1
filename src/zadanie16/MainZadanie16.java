package zadanie16;

import java.util.Scanner;

public class MainZadanie16 {
    public static void main(String[] args) {
        Scanner konsola = new Scanner(System.in);

        int liczba = 1;
//        do {
//            liczba = konsola.nextInt();
//            if (liczba <= 0) break;
//            System.out.println("Hello World!");
//        } while (liczba > 0);

        while (liczba > 0) {
            liczba = konsola.nextInt();
            if (liczba <= 0) break;
            System.out.println("Hello World!");
        }

    }
}
