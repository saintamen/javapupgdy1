package zadanie15;

import java.util.Scanner;

public class MainZadanie15 {
    public static void main(String[] args) {
        Scanner konsola = new Scanner(System.in);
        System.out.println("Ile razy?");
        int iloscRazy = konsola.nextInt();

        // uzywajac for
        for (int i = 0; i < iloscRazy; i++) {
            System.out.println("Hello World!");
        }

        System.out.println();
        // uzywajac while
        while ((iloscRazy--) > 0) {
            System.out.println("Hello World!");
        }
    }
}
