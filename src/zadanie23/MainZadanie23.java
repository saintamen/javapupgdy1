package zadanie23;

import java.util.Scanner;

public class MainZadanie23 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int liczba = scanner.nextInt();

        // 1 sie nie liczy (liczba pierwsza jest podzielna przez 1 i
        // samą siebie)
        boolean jestPierwsza = true;
        for (int i = 2; i <liczba ; i++) {
            if(liczba%i == 0){
                jestPierwsza = false;
            }
        }

        if (jestPierwsza){
            System.out.println("Jest pierwsza");
        }else{
            System.out.println("Jest złożona");
        }
    }
}
