package zadanie13;

import java.util.Scanner;

public class MainZadanie13 {
    /**
     * Napisać program pobierający od użytkownika dwie liczby całkowite A oraz
     * B, A < B, a następnie wyznaczający sumę ciągu liczb od A do B, czyli
     * sumę ciągu (A,A + 1,...,B).
     * Obliczenia należy wykonać dwukrotnie stosując kolejno pętle: while, for.
     Przykład: Dla A = 4 i B = 11 program powinien wyświetlić: 60 60
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe A:");
        int A = sc.nextInt();
        System.out.println("Podaj liczbe B:");
        int B = sc.nextInt();

        // a ((A,A + 1,...,B))
        // (A,A + 1,...,B-1) to petla wygladalaby for (int i = A; i < B; i++) {
        //
        int sum = 0;
        for (int i = A; i <= B; i++) { // przedział domknięty
            sum += i;
        }
        System.out.println("Suma = " + sum);

        // b
        int i = A;
        int sum2 = 0;
        while (i <= B){
            sum2 += i;
            i++;
        }
        System.out.println("Suma2 = " + sum2);
    }
}
