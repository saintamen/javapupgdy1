package zadanie12;

import java.util.Scanner;

public class MainZadanie12 {
    public static void main(String[] args) {
        Scanner konsola = new Scanner(System.in);

        // a
        System.out.println("Podaj zakres:");
        int zakres = konsola.nextInt();
        for (int i = 0; i <= zakres; i++) {
            if (i % 2 == 1) { // jeśli liczba jest nieparzysta
                System.out.println(i);
            }
        }
        // b
        for (int i = 3; i < 100; i++) {
            if (((i % 3) == 0) || ((i % 5) == 0)) {
                System.out.println(i);
            }
        }
        // c
        int zakresDol = konsola.nextInt();  // wczytaj liczby (zakres)
        int zakresGorny = konsola.nextInt();
        for (int i = zakresDol; i < zakresGorny; i++) { // iteruj od zakres dol do zakres gora
            if(i % 6 == 0){ // wypisz liczby podzielne przez 6
                System.out.println(i);
            }
        }
    }
}
