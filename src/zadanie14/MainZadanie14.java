package zadanie14;

public class MainZadanie14 {
    public static void main(String[] args) {
        // 'a' w kodzie ASCII = 97
        // 'z' w kodzie ASCII = 122
        for (int i = 97; i <= 122; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        // wypiszą się cyfry
        for (int i = 97; i <= 122; i++) {
            System.out.print((char)i + " ");
        }
        //
        System.out.println();
        for (char i = 97; i <= 122; i++) {
            System.out.print(i + " ");
        }
        //
        System.out.println();
        for (char i = 'a'; i <= 'z'; i++) {
            System.out.print(i + " ");
        }
    }
}
