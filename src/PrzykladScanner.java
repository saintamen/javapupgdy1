import java.util.Scanner;

public class PrzykladScanner {
    public static void main(String[] args) {
        // deklaracja
        int a = 1231515;
        long wynik = a*123123;

        Scanner scanner = new Scanner(System.in);
        System.out.println();
//        int zmiennaint1 = scanner.nextInt(); // pobranie z konsoli 1 int'a
//        int zmiennaint2 = scanner.nextInt(); // pobranie z konsoli 1 int'a


        System.out.println("Podaj wiek:");
        String liniaWiek = scanner.nextLine(); // wczytuje linie do entera
//        String linia2 = scanner.nextLine(); // wczytuje linie do entera
//        String linia3 = scanner.nextLine(); // wczytuje linie do entera
//        int == Integer
        try {
            int wiek = Integer.parseInt(liniaWiek);

            System.out.println("Twój wiek: " + wiek);
        }catch (NumberFormatException nfe){
            System.out.println("Zly format wieku");
        }

//        String slowo = scanner.next(); // wczytuje slowo do spacji
    }
}
