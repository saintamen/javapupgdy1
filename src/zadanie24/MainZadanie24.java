package zadanie24;

import java.util.Random;
import java.util.Scanner;


public class MainZadanie24 {
    public static void main(String[] args) {

        Random generator = new Random();
        int iloscLiczb = 10;
        int przedzialGora = 10;
        int przedzialDol = -10;
        int rozmiarPrzedzialu = przedzialGora - przedzialDol;

        // roznica miedzy zadaniem 18  (tak było w 18)
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj ilosc liczb:");
//        int iloscLiczb = scanner.nextInt();


        //stworzenie tablicy rozmiaru 'iloscLiczb'
        int[] tablica = new int[iloscLiczb];

        // ładowanie liczb do tablicy
        for (int i = 0; i < iloscLiczb; i++) {
            // wylosuj liczbe z zakresu 0-20, odejmij od niej 10
            tablica[i] = generator.nextInt(rozmiarPrzedzialu) + przedzialDol;
            // w 18 byl scanner;
//            tablica[i] = scanner.nextInt();
        }

        int max = tablica[0];
        int min = tablica[0];
        double sum = 0;
        for (int i = 0; i < iloscLiczb; i++) {
            // sprawdzamy max
            if (tablica[i] > max) {
                max = tablica[i];
            }
            // sprawdzamy min
            if (tablica[i] < min) {
                min = tablica[i];
            }
            // sumujemy do sredniej (na potem)
            sum += tablica[i];
        }
        double srednia = (sum / iloscLiczb);
        System.out.println("Minimum = " + min);
        System.out.println("Maximum = " + max);
        System.out.println("Min+max = " + (min + max));
        System.out.println("Srednia = " + srednia);

        int liczbaWiekszych = 0;
        int liczbaMniejszych = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] < srednia) {
                liczbaMniejszych++;
            }
            if (tablica[i] > srednia) {
                liczbaWiekszych++;
            }
        }
        System.out.println("Wiekszych:  " + liczbaWiekszych);
        System.out.println("Mniejszych: " + liczbaMniejszych);
    }
}
