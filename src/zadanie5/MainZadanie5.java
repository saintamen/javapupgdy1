package zadanie5;

public class MainZadanie5 {
    public static void main(String[] args) {
        int waga = 50;
        int wzrost = 10;
        int wiek = 15;

        // 5a/5b/5c
        // jeśli spełniony jest którykolwiek z warunków
        // to pozostałe nie są nawet sprawdzane
        if(wiek < 10 || wiek > 80){ // warunki wykluczające
            // jeśli spełniony jest warunek wyzej
            System.out.println("Nie możesz wejść z powodu wieku");
        }else if(wzrost > 220 || wzrost < 150 ){    // warunki wykluczające
            System.out.println("Nie możesz wejść z powodu wzrostu");
        }else if(waga > 180){   // warunki wykluczające
            System.out.println("Nie możesz wejść z powodu wagi");
        }else{      // nie ma powodów żeby dana osoba nie mogła wejść na kolejke
            System.out.println("Możesz wejść");
        }

        // przed modyfikacją do 5a, 5b, 5c
//        if(waga < 180 && wzrost > 150){
//            System.out.println("Możesz wejść");
//        }else{
//            System.out.println("Nie możesz wejść");
//            System.out.printf("Nie możesz wejść %.3f", wzrost);
//        }


    }
}
