package zadanie29;

import java.util.Scanner;

public class MainZadanie29 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String slowo = sc.next();

        char ostatniaLitera = slowo.charAt(slowo.length() - 1);

        int licznikLiter = 0;
        for (int i = 0; i < slowo.length(); i++) {
            if (ostatniaLitera == slowo.charAt(i)) {
                licznikLiter++;
            }
        }
        System.out.println("Ilosc wystapien: " + licznikLiter);
    }
}
