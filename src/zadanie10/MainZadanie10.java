package zadanie10;

import java.util.Scanner;

public class MainZadanie10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj kwotę dochodu:");
        double dochod = sc.nextDouble();
        if (dochod > 85528) {
            // jeśli dochód wynosi od 85.528 podatek wynosi 14.839,02 zł + 32% nadwyżki ponad 85.528,00s
            double nadwyzka = dochod - 85528;
            double podatek = 14839.02 + (.32 * nadwyzka);
            System.out.println("Podatek: " + podatek);
        } else {
            // jeśli dochód wynosi do 85.528 podatek wynosi 18% podstawy minus 556,02 PLN,
            double podatek = dochod * .18 - 556.02;
            System.out.println("Podatek: " + podatek);

        }
    }
}
