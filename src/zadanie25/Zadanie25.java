package zadanie25;

import java.util.Random;

public class Zadanie25 {
    public static void main(String[] args) {
        Random generator = new Random();
        int iloscLiczb = 20;
        int minPrzedzial = 1;
        int maxPrzedzial = 10;

        int dlugoscPrzedzialu = maxPrzedzial - minPrzedzial;
        int[] tablica = new int[iloscLiczb];
        for (int i = 0; i < tablica.length; i++) {
            // generujemy liczby
            tablica[i] = generator.nextInt(dlugoscPrzedzialu + 1) + minPrzedzial;
        }

        // wypisanie wszystkich liczb
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + ", ");
        }

        int[] wystapienia = new int[maxPrzedzial + 1]; // dla kazdego indeksu
        // z tej tablicy pod numerem indeksu (w komorce)
        // bedzie sie znajdowac ilosc wystapien tej liczby
        // tzn. tej = o tym indeksie

        for (int i = 0; i < tablica.length; i++) {
            int liczba = tablica[i];
            wystapienia[liczba]++;
        }

        for (int i = 0; i < wystapienia.length; i++) {
            System.out.println(i + " - " + wystapienia[i]);
        }


    }
}
