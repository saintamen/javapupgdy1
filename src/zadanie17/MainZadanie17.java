package zadanie17;

import java.util.Scanner;

public class MainZadanie17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int liczbaCalkowita = scanner.nextInt();

        double wynikPotegowania = 1.0;
        int i = 1; // aby inkrementować numery potęg
        do {
            System.out.println(wynikPotegowania); // zawsze wypisujemy 1.0
            wynikPotegowania = Math.pow(2, i++);
        } while (wynikPotegowania < liczbaCalkowita);

        // z samym mnożeniem

        double wynik = 1.0;
        do{
            System.out.println(wynik);
            wynik*=2;
        }while (wynik<liczbaCalkowita);
    }
}
