package zadanie20;

import java.util.Random;
import java.util.Scanner;

public class MainZadanie20 {
    public static void main(String[] args) {
        Random generator = new Random();
        Scanner scanner = new Scanner(System.in);
        int losowa = generator.nextInt(100) +1;

        int liczba;
        // dopoki nie trafilsmy
        while((liczba = scanner.nextInt()) != losowa){
            if(liczba > losowa){
                System.out.println("Za duzo!");
            }else{
                System.out.println("Za malo");
            }
        }
        System.out.println("Gratulacje");
    }
}
