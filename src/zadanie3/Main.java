package zadanie3;

public class Main {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int c = 3;

        // a <= b               // 2
        // b <= c               // 3
        // c <= a (ta pierwotna)// 1

        System.out.println(a + " " + b + " " + c);

        int zmiennaTymczasowa = a; // schowek
        a = b; // 2
        b = c; // 3
        c = zmiennaTymczasowa; // 2

        System.out.println(a + " " + b + " " + c);
    }

}
