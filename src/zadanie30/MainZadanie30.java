package zadanie30;

import java.util.Scanner;

public class MainZadanie30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String slowo = sc.next();

        StringBuilder builder = new StringBuilder();
//        String odwroconeSlowo = "";

        // petla od tylu slowa
        for (int i = slowo.length() - 1; i >= 0; i--) {
//            System.out.print(slowo.charAt(i));

            // to ponizej to dopisywanie litery do ciagu znakow
            //
//            odwroconeSlowo += slowo.charAt(i);

            // uzywajac buildera
            builder.append(slowo.charAt(i));
        }

//        System.out.println("Odwrocone:  " + odwroconeSlowo);
        System.out.println("Odwrocone:  " + builder.toString());


    }
}
