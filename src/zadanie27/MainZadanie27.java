package zadanie27;

import java.util.Scanner;

public class MainZadanie27 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe:");
        int liczba = sc.nextInt();

        int najwiekszaLiczba = liczba * liczba;
        int dlugoscNajwiekszejLiczby = String.valueOf(najwiekszaLiczba).length()+1;

        for (int i = 1; i <= liczba; i++) {
            for (int j = 1; j <= liczba; j++) {
                int wynik = j * i;
                // dlugosc (ilosc znakow) w liczbie 'wynik'
                int dlugoscLiczbyWynik = String.valueOf(wynik).length();

                int roznicaDlugosci = dlugoscNajwiekszejLiczby - dlugoscLiczbyWynik;
                // tyle ^ spacji musze wpisac by wyjustowac kolumny
                for (int k = 0; k < roznicaDlugosci; k++) {
                    System.out.print(" ");
                }
                // wypisanie wyniku
                System.out.print(wynik);
            }
            System.out.println();
        }
    }
}
