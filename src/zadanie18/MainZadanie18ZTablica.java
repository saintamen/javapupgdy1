package zadanie18;

import java.util.Scanner;

public class MainZadanie18ZTablica {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb:");
        int iloscLiczb = scanner.nextInt();

        //stworzenie tablicy rozmiaru 'iloscLiczb'
        int[] tablica = new int[iloscLiczb];

        // ładowanie liczb do tablicy
        for (int i = 0; i < iloscLiczb; i++) {
            tablica[i] = scanner.nextInt();
        }

        int max = tablica[0];
        int min = tablica[0];
        double sum = 0;
        for (int i = 0; i < iloscLiczb; i++) {
            // sprawdzamy max
            if (tablica[i] > max) {
                max = tablica[i];
            }
            // sprawdzamy min
            if (tablica[i] < min) {
                min = tablica[i];
            }
            // sumujemy do sredniej (na potem)
            sum += tablica[i];
        }
        double srednia = (sum / iloscLiczb);
        System.out.println("Min+max = " + (min + max));
        System.out.println("Srednia = " + srednia);
    }
}
