package zadanie9;

import java.util.Scanner;

public class MainZadanie9 {
    public static void main(String[] args) {
        Scanner konsola = new Scanner(System.in);
        System.out.println("Podaj wage:");
        int waga = konsola.nextInt();
        System.out.println("Podaj wzrost:");
        int wzrost = konsola.nextInt();
        System.out.println("Podaj wiek:");
        int wiek = konsola.nextInt();

        // 5a/5b/5c
        // jeśli spełniony jest którykolwiek z warunków
        // to pozostałe nie są nawet sprawdzane
        if(wiek < 10 || wiek > 80){ // warunki wykluczające
            // jeśli spełniony jest warunek wyzej
            System.out.println("Nie możesz wejść z powodu wieku");
        }else if(wzrost > 220 || wzrost < 150 ){    // warunki wykluczające
            System.out.println("Nie możesz wejść z powodu wzrostu");
        }else if(waga > 180){   // warunki wykluczające
            System.out.println("Nie możesz wejść z powodu wagi");
        }else{      // nie ma powodów żeby dana osoba nie mogła wejść na kolejke
            System.out.println("Możesz wejść");
        }

        // przed modyfikacją do 5a, 5b, 5c
//        if(waga < 180 && wzrost > 150){
//            System.out.println("Możesz wejść");
//        }else{
//            System.out.println("Nie możesz wejść");
//            System.out.printf("Nie możesz wejść %.3f", wzrost);
//        }


    }
}
